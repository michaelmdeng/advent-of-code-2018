// ---------------
// Common settings
// ---------------
name := "AdventOfCode"

fork := true

// -------------------
// Dependency settings
// ----------ß---------

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "1.1.0",
  "com.jsuereth" %% "scala-arm" % "2.0"
  )
